Proyecto de la 3º evaluación de Programación, migración de un proyecto ya realizado en JQuery pasarlo a AngularJS

Hecho:
	- Contact
	- Main, list, paginador, autocomplete y categories
	- Modulo Google Maps
	- Details
	- Login Manual
	- Login RRSS
	- Profile
	- Likes y comentarios
	- Perfil, aparecen los comentarios y los likes

Pendiente:
	- Sección de amigos
	

Mejoras:
	- Funciones de limpieza de span
	- Module de Google Maps
	- Factorias de servicios
	- Función que detecta el evento Intro, en un input
	- No se puede dar 2 veces like a la misma noticia
	- Una vez enviado el comentario, aparece al momento ya insertado en la noticia
	- Que aparezcan el numero de "Me gusta" en los detalles de la noticia
	- Perfil con sistema de pestañas, una pestaña para cambiar el perfil y otra donde aparecen todos sus registros de likes y comentarios