-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 11-06-2018 a las 10:17:03
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `avatonews`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `cod` varchar(5) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`cod`, `name`) VALUES
('CUL', 'Cultura'),
('DEP', 'Deportes'),
('ECO', 'Economia'),
('ESP', 'Espanya'),
('INT', 'Internacional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id_new` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `date_` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comments`
--

INSERT INTO `comments` (`id_new`, `user`, `message`, `date_`, `photo`) VALUES
('3456', 'Alejandro', 'fafsfasfa', '2018-06-11 11:10:04', 'http://127.0.0.1//FW_PHP_OO_AngularJS_AvatoNews/backend/media/maria-sharapova-small.png'),
('3456', 'Alejandro', 'safafwq', '2018-06-11 11:10:07', 'http://127.0.0.1//FW_PHP_OO_AngularJS_AvatoNews/backend/media/maria-sharapova-small.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id_new` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `date_` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_new`,`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `likes`
--

INSERT INTO `likes` (`id_new`, `user`, `date_`) VALUES
('3456', 'Alejandro', '2018-06-11 12:10:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `author` varchar(25) DEFAULT NULL,
  `id_new` varchar(100) NOT NULL,
  `headline` varchar(255) DEFAULT NULL,
  `event_date` varchar(12) DEFAULT NULL,
  `publication_date` varchar(12) DEFAULT NULL,
  `cat1` varchar(20) DEFAULT NULL,
  `cat2` varchar(20) DEFAULT NULL,
  `cat3` varchar(20) DEFAULT NULL,
  `cat4` varchar(20) DEFAULT NULL,
  `cat5` varchar(20) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `path` varchar(50) DEFAULT NULL,
  `main_group` varchar(4) DEFAULT NULL,
  `second_group` int(11) DEFAULT NULL,
  `specific_cat` int(11) DEFAULT NULL,
  `trending` int(11) DEFAULT NULL,
  `longitud` varchar(255) DEFAULT NULL,
  `latitud` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_new`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `news`
--

INSERT INTO `news` (`author`, `id_new`, `headline`, `event_date`, `publication_date`, `cat1`, `cat2`, `cat3`, `cat4`, `cat5`, `category`, `path`, `main_group`, `second_group`, `specific_cat`, `trending`, `longitud`, `latitud`) VALUES
('Yester', '12345', 'Victoria aplastante', '17/03/2018', '20/03/2018', '', 'Internacional', '', '', 'Deportes', 'Portada', 'valenciacf', 'DEP', 1, 7, NULL, '-0.3530287082153427', '39.46665469132515'),
('Yester', '3456', 'oubasf', '13/03/2018', '23/03/2018', '', 'Internacional', '', '', '', 'Portada', 'asfa', 'CUL', 4, 3, NULL, '-0.3745643743746774', '39.47543741274128'),
('Yester', '41241', 'Prueba123', '02/04/2018', '17/04/2018', 'EconomÃ­a', 'Internacional', '', '', '', 'Portada', 'adasdassd', NULL, NULL, NULL, NULL, '-0.4084512654612544', '39.474521645676425'),
('Uhgv', 'ADAd', 'FASF', '12/04/2018', '20/04/2018', '', '', 'Cultura', '', '', 'Portada', 'HGDF', NULL, NULL, NULL, NULL, '-0.3430287082153429', '39.45665469133515'),
('Lin', 'afasf', ' kadv av s', '06/04/2018', '21/04/2018', 'EconomÃ­a', '', '', '', '', 'Portada', 'dsadasd', NULL, NULL, NULL, NULL, '-0.3430287092153427', '39.45665469142515'),
('Yhfsaf', 'asfas', 'fssafas', '12/04/2018', '21/04/2018', '', '', '', '', 'Deportes', 'Portada', 'sfaf', NULL, NULL, NULL, NULL, '-0.3430287082157427', '39.45665469232515'),
('Tgdas', 'dasd', 'sada', '07/04/2018', '18/04/2018', '', '', '', 'EspaÃ±a', '', 'Portada', 'dasdad', NULL, NULL, NULL, NULL, '-0.3430287082163427', '39.45665479132515'),
('Rf', 'dsad', 'das', '12/03/2018', '22/03/2018', 'EconomÃ­a', 'Internacional', '', '', '', 'Portada', 'adsdas', 'INT', 1, 3, NULL, '-0.3430287082193427', '37.901721555657428'),
('Ygdas', 'dsadasdd', 'safas', '10/04/2018', '19/04/2018', 'EconomÃ­a', '', '', '', '', 'Portada', 'fasfsa', NULL, NULL, NULL, NULL, '-0.3430287082163427', '39.45665569132515'),
('Edg', 'fasf', 'fsa', '11/04/2018', '20/04/2018', '', 'Internacional', '', '', '', 'Portada', 'fsafas', NULL, NULL, NULL, NULL, '-0.3430287082155427', '37.901721555667424'),
('Dfb', 'fasfa', 'fsa', '13/03/2018', '24/03/2018', 'EconomÃ­a', 'Internacional', '', '', '', 'Portada', 'fsafa', 'ESP', 3, 4, NULL, NULL, NULL),
('Yb', 'fsa', 'sfa', '13/03/2018', '23/03/2018', 'EconomÃ­a', 'Internacional', '', '', '', 'Portada', 'fasfa', 'ECO', 2, 3, NULL, NULL, NULL),
('Tbnm', 'fsafa', 'sfa', '11/04/2018', '21/04/2018', 'EconomÃ­a', '', '', '', '', 'Portada', 'fasfas', NULL, NULL, NULL, NULL, NULL, NULL),
('Ihbf', 'fsafasf', 'fasf', '10/04/2018', '19/04/2018', 'EconomÃ­a', '', '', '', '', 'Portada', 'sfasf', NULL, NULL, NULL, NULL, NULL, NULL),
('Edf', 'gds', 'gds', '12/03/2018', '22/03/2018', 'EconomÃ­a', '', '', '', '', 'Portada', 'gsgds', 'DEP', 1, 3, NULL, NULL, NULL),
('Lin', 'jnakf', ' kadv av s', '06/04/2018', '21/04/2018', 'EconomÃ­a', '', '', '', '', 'Portada', 'dsadasd', NULL, NULL, NULL, NULL, NULL, NULL),
('Sfaa', 'sfasf', 'sfa', '11/04/2018', '19/04/2018', '', 'Internacional', '', '', '', 'Portada', 'fsafasf', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `specific_cat`
--

CREATE TABLE IF NOT EXISTS `specific_cat` (
  `cod_cat` varchar(5) NOT NULL,
  `cod_sub` varchar(5) NOT NULL,
  `cod` int(11) NOT NULL,
  `concrete_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`cod`,`cod_cat`,`cod_sub`),
  KEY `FK_sub` (`cod_sub`),
  KEY `FK_cat_specific` (`cod_cat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `specific_cat`
--

INSERT INTO `specific_cat` (`cod_cat`, `cod_sub`, `cod`, `concrete_name`) VALUES
('CUL', '1', 1, 'Accion'),
('CUL', '2', 1, 'Rock'),
('CUL', '3', 1, 'Policiaca'),
('CUL', '4', 1, 'Manga'),
('DEP', '1', 1, 'Athletic de Bilbao'),
('DEP', '2', 1, 'Real Madrid Baloncesto'),
('DEP ', '4', 1, 'League of Legends'),
('DEP', '5', 1, 'Ferrari'),
('ECO', '1', 1, 'Banca'),
('ECO', '2', 1, 'Bolsa'),
('ECO', '3', 1, 'Ibex35'),
('ECO', '4', 1, 'Consejos'),
('ECO', '5', 1, 'Precio'),
('ESP', '1', 1, 'Valencia'),
('ESP', '2', 1, 'Barcelona'),
('ESP', '3', 1, 'Malaga'),
('ESP', '5', 1, 'Teruel'),
('INT', '1', 1, 'Argentina'),
('INT', '2', 1, 'China'),
('INT', '3', 1, 'Sudafrica'),
('INT', '4', 1, 'Portugal'),
('INT', '5', 1, 'Australia'),
('CUL', '1', 2, 'Romantica'),
('CUL', '2', 2, 'Metal'),
('CUL', '3', 2, 'Fantastica'),
('CUL', '4', 2, 'Comic'),
('DEP', '1', 2, 'Atletico de Madrid'),
('DEP', '2', 2, 'Saski Baskonia'),
('DEP', '4', 2, 'Overwatch'),
('DEP', '5', 2, 'Mercedez'),
('ECO', '1', 2, 'IPC'),
('ECO', '2', 2, 'ERE'),
('ECO', '3', 2, 'DowJones'),
('ECO', '4', 2, 'Auge'),
('ECO', '5', 2, 'Plan vivienda'),
('ESP', '1', 2, 'Alicante'),
('ESP', '2', 2, 'Girona'),
('ESP', '3', 2, 'Huelva'),
('ESP', '5', 2, 'Zaragoza'),
('INT', '1', 2, 'Brasil'),
('INT', '2', 2, 'Korea del Sur'),
('INT', '3', 2, 'Egipto'),
('INT', '4', 2, 'Francia'),
('INT', '5', 2, 'Indonesia'),
('CUL', '1', 3, 'Ciencia Ficcion'),
('CUL', '2', 3, 'Clasica'),
('CUL', '3', 3, 'Misterio'),
('CUL', '4', 3, 'Arte'),
('DEP', '1', 3, 'FC Barcelona'),
('DEP', '2', 3, 'Futbol Club Barcelona'),
('DEP', '4', 3, 'DOTA2'),
('DEP', '5', 3, 'Mc-Laren Renault'),
('ECO', '1', 3, 'Pensiones'),
('ECO', '2', 3, 'Sector primario'),
('ECO', '3', 3, 'Nasdaq'),
('ECO', '4', 3, 'Planes de pensiones'),
('ECO', '5', 3, 'Hipotecas'),
('ESP', '1', 3, 'Castellon'),
('ESP', '2', 3, 'Tarragona'),
('ESP', '3', 3, 'Cadiz'),
('ESP', '5', 3, 'Huesca'),
('INT', '1', 3, 'Colombia'),
('INT', '2', 3, 'Korea del Norte'),
('INT', '3', 3, 'Marruecos'),
('INT', '4', 3, 'Inglaterra'),
('INT', '5', 3, 'Fiyi'),
('CUL', '1', 4, 'Humor'),
('CUL', '2', 4, 'Pop'),
('CUL', '3', 4, 'Humor'),
('CUL', '4', 4, 'Animacion'),
('DEP', '1', 4, 'Real Madrid'),
('DEP', '2', 4, 'Unicaja Baloncesto'),
('DEP', '4', 4, 'Heros of the Storm'),
('DEP', '5', 4, 'Renault'),
('ECO', '1', 4, 'PIB'),
('ECO', '2', 4, 'Sector secundario'),
('ESP', '2', 4, 'Lleida'),
('ESP', '3', 4, 'Jaen'),
('INT', '1', 4, 'Peru'),
('INT', '2', 4, 'Japon'),
('INT', '3', 4, 'Tunez'),
('INT', '4', 4, 'Italia'),
('INT', '5', 4, 'Islas Caiman'),
('CUL', '1', 5, 'Miedo'),
('CUL', '2', 5, 'Celta'),
('DEP', '1', 5, 'Sevilla'),
('DEP', '2', 5, 'Valencia Basket'),
('DEP', '4', 5, 'Hearthstone'),
('DEP', '5', 5, 'Red Bull'),
('ECO', '2', 5, 'Sector terciario'),
('ESP', '3', 5, 'Cordoba'),
('INT', '1', 5, 'Uruguay'),
('INT', '2', 5, 'India'),
('INT', '3', 5, 'Nigeria'),
('INT', '4', 5, 'Alemania'),
('CUL', '1', 6, 'Suspense'),
('CUL', '2', 6, 'Vinking'),
('DEP', '1', 6, 'Real Betis'),
('DEP', '2', 6, 'Basket Zaragoza'),
('DEP', '4', 6, 'CSGO'),
('DEP', '5', 6, 'Toro Rosso'),
('ESP', '3', 6, 'Granada'),
('INT', '1', 6, 'Paraguay'),
('INT', '3', 6, 'Etiopia'),
('INT', '4', 6, 'Suiza'),
('DEP', '1', 7, 'Valencia CF'),
('DEP', '2', 7, 'Club Estudiantes'),
('DEP', '4', 7, 'FIFA'),
('DEP', '5', 7, 'Force Indians'),
('ESP', '3', 7, 'Sevilla'),
('INT', '1', 7, 'Mexico'),
('INT', '3', 7, 'Ghana'),
('INT', '4', 7, 'Suecia'),
('DEP', '1', 8, 'Girona'),
('DEP', '5', 8, 'Sauber'),
('INT', '1', 8, 'EEUU'),
('INT', '3', 8, 'Costa de Marfil'),
('INT', '4', 8, 'Rusia'),
('DEP', '1', 9, 'Villareal'),
('DEP', '5', 9, 'Williams'),
('INT', '1', 9, 'Canada'),
('INT', '4', 9, 'Grecia'),
('DEP', '1', 10, 'Getafe'),
('INT', '1', 10, 'Chile'),
('DEP', '1', 11, 'Celta de Vigo'),
('INT', '1', 11, 'Ecuador'),
('DEP', '1', 12, 'Deportivo de la Corunya'),
('DEP', '1', 13, 'Las Palmas'),
('DEP', '1', 14, 'Levante'),
('DEP', '1', 15, 'Malaga'),
('DEP', '1', 16, 'Real Sociedad'),
('DEP', '1', 17, 'Eibar'),
('DEP', '1', 18, 'Leganes'),
('DEP', '1', 19, 'Alaves'),
('DEP', '1', 20, 'Espanyol');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategory`
--

CREATE TABLE IF NOT EXISTS `subcategory` (
  `cod_cat` varchar(5) NOT NULL,
  `cod` varchar(5) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`cod`,`cod_cat`),
  KEY `FK_cat` (`cod_cat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subcategory`
--

INSERT INTO `subcategory` (`cod_cat`, `cod`, `name`) VALUES
('CUL', '1', 'Cine'),
('DEP', '1', 'Futbol'),
('ECO', '1', 'Macroeconomia'),
('ESP', '1', 'C.Valenciana'),
('INT', '1', 'America'),
('CUL', '2', 'Musica'),
('DEP', '2', 'Baloncesto'),
('ECO', '2', 'Empresas'),
('ESP', '2', 'Catalunya'),
('INT', '2', 'Asia'),
('CUL', '3', 'Literatura'),
('DEP', '3', 'Ciclismo'),
('ECO', '3', 'Bolsa'),
('ESP', '3', 'Andalucia'),
('INT', '3', 'Africa'),
('CUL', '4', 'Comic'),
('DEP', '4', 'E-Sports'),
('ECO', '4', 'Ahorro y consumo'),
('ESP', '4', 'Catalunya'),
('INT', '4', 'Europa'),
('DEP', '5', 'F1'),
('ECO', '5', 'Vivienda'),
('ESP', '5', 'Aragon'),
('INT', '5', 'Oceania');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(255) DEFAULT NULL,
  `name_user` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `sign_in` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type_user` varchar(255) DEFAULT NULL,
  `confirmed` tinyint(1) DEFAULT NULL,
  `password_user` varchar(255) DEFAULT NULL,
  `original_name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `token2` varchar(255) DEFAULT NULL,
  `date_birthday` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `town` varchar(255) DEFAULT NULL,
  `real_surname` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name_user`, `email`, `photo`, `sign_in`, `token`, `type_user`, `confirmed`, `password_user`, `original_name`, `surname`, `token2`, `date_birthday`, `country`, `province`, `town`, `real_surname`) VALUES
('278098678', 'Alejandro VaÃ±Ã³ TomÃ¡s', '', 'http://127.0.0.1//FW_PHP_OO_AngularJS_AvatoNews/backend/media/maria-sharapova-small.png', '2018-05-30 21:47:43', 'accdcb0e798dea89192e655582a8e6e7', 'reader', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('108672391394571534573', 'Alejandro VaÃ±Ã³ TomÃ¡s', 'avato92@gmail.com', 'http://127.0.0.1//FW_PHP_OO_AngularJS_AvatoNews/backend/media/maria-sharapova-small.png', '2018-05-30 21:47:54', 'b9cf5ed7724192fa98a62ee534255679', 'reader', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('10214249120235052', 'Alejandro VaÃ±Ã³ TomÃ¡s', 'yesterquiff@hotmail.com', 'http://127.0.0.1//FW_PHP_OO_AngularJS_AvatoNews/backend/media/maria-sharapova-small.png', '2018-05-31 19:25:06', NULL, 'reader', 1, NULL, NULL, NULL, '309802543a17f081ccb694b8397171e6', NULL, NULL, NULL, NULL, NULL),
(NULL, 'Alejandro', 'avato92@gmail.com', 'http://127.0.0.1//FW_PHP_OO_AngularJS_AvatoNews/backend/media/maria-sharapova-small.png', '2018-06-05 18:21:42', 'Cha794eff6d542a4cbbe4dbbec636ed7131', 'reader', 1, '$2y$10$SS0Bqj25828hYCH1hWwKVOBCGBDPPjdNHdG9QHuDDG/7FUjK6has.', 'adfsfasf', 'sdadasdasd', '1314621abcbf5683489cc3677ed5dccf', '05/06/2018', 'Spain', 'Castellon', 'Alquerias De Santa Barbara', NULL);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `specific_cat`
--
ALTER TABLE `specific_cat`
  ADD CONSTRAINT `FK_cat_specific` FOREIGN KEY (`cod_cat`) REFERENCES `category` (`cod`),
  ADD CONSTRAINT `FK_sub` FOREIGN KEY (`cod_sub`) REFERENCES `subcategory` (`cod`);

--
-- Filtros para la tabla `subcategory`
--
ALTER TABLE `subcategory`
  ADD CONSTRAINT `FK_cat` FOREIGN KEY (`cod_cat`) REFERENCES `category` (`cod`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
