<?php

class controller_international {
    function __construct() {
        $_SESSION['module'] = "international";
    }

    function map() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/international/view/', 'map.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function list_news() {
    	$coordenadas = $_POST['key'];
        $json = array();

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/international/model/model/';
        $json = loadModel($path_model, "international_model", "search_news", $coordenadas);
        echo json_encode($json);
        exit();
    }

    function details_new() {
        $_SESSION['id'] = $_GET['aux'];
      
         require_once(VIEW_PATH_INC . "header.php");
         require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/international/view/', 'details.html');

         require_once(VIEW_PATH_INC . "footer.html");
    }

    function search_details() {

        $id = $_GET['aux'];
        $json = array();

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/international/model/model/';
        $json = loadModel($path_model, "international_model", "search_details", $id);
        echo json_encode($json);
        exit();
    }

    function list_all_news() {
        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/international/model/model/';
        $json = loadModel($path_model, "international_model", "pull_news");
        echo json_encode($json);
        exit();
    }

    function comments(){
 
        if(isset($_GET['aux'])){
            $json = array();
            $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/international/model/model/';
            $json = loadModel($path_model, "international_model", "pull_comments", $_GET['aux']);


            if($json != "error"){
                echo json_encode($json);
                exit();
            }else{
                echo json_encode($json);
                exit();
            }
        }
    }

    function new_comment(){
        $user = $_POST['user'];

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/international/model/model/';
            $user = loadModel($path_model, "international_model", "pull_user", $user);

            if($user != 'error'){
                $info = array(
                    'user' => $user[0]['name_user'],
                    'comment' => $_POST['comment'],
                    'photo' => $user[0]['photo'],
                    'id_new' => $_POST['id_new']
                );
                $confirm = loadModel($path_model, "international_model", "push_comment", $info);

                if($confirm == true){
                    $jsondata['success'] = true;
                    echo json_encode($jsondata);
                    exit();
                }else{
                    $jsondata['success'] = false;
                    echo json_encode($jsondata);
                    exit();
                }
            }else{
                $jsondata['success'] = false;
                echo json_encode($jsondata);
                exit();               
            }
    }

    function like_this(){
       $user = $_POST['user'];

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/international/model/model/';
        $user = loadModel($path_model, "international_model", "pull_user", $user);

        if($user != 'error'){
                $info = array(
                    'user' => $user[0]['name_user'],
                    'id_new' => $_POST['id_new']
                );
            $confirm = loadModel($path_model, "international_model", "like_this", $info);

            if($confirm == true){
                    $jsondata['success'] = true;
                    echo json_encode($jsondata);
                    exit();
                }else{
                    $jsondata['success'] = false;
                    $jsondata['error'] = 2;
                    echo json_encode($jsondata);
                    exit();
                }
        }else{
            $jsondata['success'] = false;
            $jsondata['error'] = 1;
            echo json_encode($jsondata);
            exit();             
        } 
    }

    function number_likes(){

       $id_new = $_GET['aux'];
        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/international/model/model/';
        $likes = loadModel($path_model, "international_model", "number_likes", $id_new);
        $number = $likes[0]["COUNT(*)"];
        echo json_encode($number);
        exit();

    }

    function rank_news(){
        $token = $_POST['user'];
        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/international/model/model/';
        $user = loadModel($path_model, "international_model", "pull_user", $token);

        $info = array(
            'user' => $user[0]['name_user'],
            'id' => $_POST['id'],
            'rank' => $_POST['rank']
        );
        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/international/model/model/';
        $confirm = loadModel($path_model, "international_model", "rank_news", $info);

        echo json_encode($confirm);
        exit();

    }

    function rank(){
        $id = $_GET['aux'];

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/international/model/model/';
        $rank = loadModel($path_model, "international_model", "rank", $id);
        $stars['number_ranks'] = $rank[0]["COUNT(*)"];
        $stars['stars'] = $rank[0]["AVG(ranking)"];
        echo json_encode($stars);
        exit();
    }
}