<?php

class international_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = international_DAO::getInstance();
        $this->db = Db::getInstance();
    }
    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function search_news_BLL($coordenadas){
      return $this->dao->search_news_DAO($this->db, $coordenadas);
    }
    public function search_details_BLL($id){
        return $this->dao->search_details_DAO($this->db, $id);
    }
    public function pull_comments_BLL($id){
        return $this->dao->pull_comments_DAO($this->db, $id);
    }
    public function push_comment_BLL($info){
        return $this->dao->push_comment_DAO($this->db, $info);
    }
    public function pull_user_BLL($token){
        return $this->dao->pull_user_DAO($this->db, $token);
    }
    public function like_this_BLL($info){
        return $this->dao->like_this_DAO($this->db, $info);
    }
    public function number_likes_BLL($id){
        return $this->dao->number_likes_DAO($this->db, $id);
    }
    public function rank_news_BLL($info){
        return $this->dao->rank_news_DAO($this->db, $info);
    }
    public function rank_BLL($id){
        return $this->dao->rank_DAO($this->db, $id);
    }
}