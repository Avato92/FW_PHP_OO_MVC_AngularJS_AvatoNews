<?php

class international_DAO {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function search_news_DAO($db, $coordenadas){
        $sql = "SELECT * FROM news WHERE longitud BETWEEN '" . $coordenadas['bounds'] . "' AND '" . $coordenadas['bounds2'] . "' AND latitud BETWEEN '" . $coordenadas['bounds3'] . "' AND '" . $coordenadas['bounds4'] . "'";
        return $db->listar($db,$sql);
    }

    public function search_details_DAO($db, $id){
        $sql = "SELECT * FROM news WHERE id_new = '$id'";

        return $db->listar($db,$sql);
    }
    public function pull_comments_DAO($db, $id){
        $sql = "SELECT * FROM comments WHERE id_new = '$id'";

        return $db->listar($db, $sql);
    }
    public function push_comment_DAO($db, $info){
        $comment = $info['comment'];
        $user = $info['user'];
        $id_new = $info['id_new'];
        $photo = $info['photo'];

        $sql = "INSERT INTO comments (id_new, user, message, date_, photo) VALUES('$id_new', '$user', '$comment', now(), '$photo') ";

        return $db->ejecutar($sql);
    }
    public function pull_user_DAO($db, $token){
        $info = $token['token'];
        $sql = "SELECT name_user, photo FROM users WHERE token2 = '$info'";
      
        return $db->listar($db, $sql); 
    }
    public function like_this_DAO($db, $info){
        $user = $info['user'];
        $id_new = $info['id_new'];

        $sql = "INSERT INTO likes (id_new, user, date_) VALUES('$id_new', '$user', now())";

        return $db->ejecutar($sql);
    }
    public function number_likes_DAO($db, $id){
        $sql = "SELECT COUNT(*) FROM likes WHERE id_new = '$id'";

        return $db->listar($db, $sql);
    }
    public function rank_news_DAO($db, $info){
        $user = $info['user'];
        $id = $info['id'];
        $rank = $info['rank'];

        $sql = "INSERT INTO rank (user, id_new, ranking) VALUES( '$user', '$id', '$rank')";

        return $db->ejecutar($sql);
    }
    public function rank_DAO($db, $id){
        $sql = "SELECT COUNT(*), AVG(ranking) FROM rank WHERE id_new = '$id'";

        return $db->listar($db, $sql);
    }
}