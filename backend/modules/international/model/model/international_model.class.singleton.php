<?php

class international_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = international_bll::getInstance();
    }
    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function search_news($coordenadas){
    	return $this->bll->search_news_BLL($coordenadas);
    }
    public function search_details($id){
        return $this->bll->search_details_BLL($id);
    }
    public function pull_comments($id){
        return $this->bll->pull_comments_BLL($id);
    }
    public function push_comment($info){
        return $this->bll->push_comment_BLL($info);
    }
    public function pull_user($token){
        return $this->bll->pull_user_BLL($token);
    }
    public function like_this($info){
        return $this->bll->like_this_BLL($info);
    }
    public function number_likes($id){
        return $this->bll->number_likes_BLL($id);
    }
    public function rank_news($info){
        return $this->bll->rank_news_BLL($info);
    }
    public function rank($id){
        return $this->bll->rank_BLL($id);
    }
}