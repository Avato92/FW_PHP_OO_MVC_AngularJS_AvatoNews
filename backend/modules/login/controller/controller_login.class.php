<?php

class controller_login {
    function __construct() {

        require_once(UTILS_PATH_LOGIN . "functions.inc.php");
        include (LIBS . 'password_compat-master/lib/password.php');
        $_SESSION['module'] = "login";
        require_once(UTILS_PATH . "mail.inc.php");
    }

    function find_social_network(){
        $user = array(
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'photo' => $_POST['photo'],
            'id' => $_POST['id']
        );

    	$count = array();

      	$path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/login/model/model/';
      	$count = loadModel($path_model, "login_model", "search_social_network_user", $user);

      	if($count){
      	     $number["exist"] = $count[0]["COUNT(*)"];
             if($number['exist'] == 0){
                $token = md5(uniqid(rand(), true));
                $user['token'] = $token;
        		$insert = loadModel($path_model, "login_model", "insert_user", $user);
        		if($insert){
        			$person = loadModel($path_model, "login_model", "select_user", $user['id']);
                    if($person){
                        $person['new'] = true;
        			     echo json_encode($person);
        			     exit();
                    }
        		}else{
        			echo json_encode("No insertado");
        			exit();
        		}
        	 }else{
                $token = md5(uniqid(rand(),true));
                $user['token'] = $token;
                $update = loadModel($path_model, "login_model", "generate_token", $user);
                if ($update == true){
        		  $person = array();
        		  $person = loadModel($path_model, "login_model", "select_user", $user['id']);
                  if ($person){
                    $person['new'] = false;
        		      echo json_encode($person);
        		      exit();
                    }
                }
        	}
      	}else{
        	$json = "error";
        	echo json_encode($json);
        	exit;
      	}

    }

    function save_user(){

        $count = array();

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/login/model/model/';
        $count = loadModel($path_model, "login_model", "search_user", $_POST['name']);
    
        if($count){
             $number["exist"] = $count[0]["COUNT(*)"];
             if($number['exist'] == 0){
                $count_email = loadModel($path_model, "login_model", "search_email", $_POST['email']);
                if($count_email){
                    $number["exist_email"] = $count_email[0]["COUNT(*)"];
                    if($number['exist_email'] == 0){
                        $photo = get_gravatar($_POST['email'], $s = 400, $d = 'identicon', $r = 'g', $img = false, $atts = array());
                        $token = "Ver" . md5(uniqid(rand(), true));
                        $token2 = md5(uniqid(rand(),true));

                        $user = array(
                            'name' => $_POST['name'],
                            'email' => $_POST['email'],
                            'password' => password_hash($_POST['password'], PASSWORD_BCRYPT),
                            'photo' => $photo,
                            'token' => $token,
                            'token2' => $token2
                        );
                        $insert = loadModel($path_model, "login_model", "insert_user_manual", $user);
                        if($insert == true){
                                $data = array(
                                    'token' => $token,
                                    'email' => $_POST['email']
                                );
                                sendtoken($data, "alta");
                    
                                $jsondata["success"] = true;
                                $jsondata["user"] = $user;
                                echo json_encode($jsondata);
                                exit();
                        }else{
                            $jsondata["success"] = false;
                            $jsondata["error"] = 3;
                            echo json_encode($jsondata);
                            exit();
                        }
                    }else{
                        $jsondata["success"] = false;
                        $jsondata["error"] = 2;
                        echo json_encode($jsondata);
                        exit();
                    }
                }
               
            }else{
                $jsondata["success"] = false;
                $jsondata["error"] = 1;
                echo json_encode($jsondata);
                exit();
            }
        }
    }

    function sign_in(){
        $user = $_POST['user'];
        $password = $_POST['password'];

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/login/model/model/';
        $pass = loadModel($path_model, "login_model", "verify_password", $user);

        if($pass != "error"){
            $confirm = password_verify($password, $pass[0]['password_user']);
            if($confirm ==  true){
                $token2 = md5(uniqid(rand(),true));
                $person['name'] = $user;
                $person['token'] = $token2;
                $update = loadModel($path_model, "login_model", "generate_token_manual", $person);
                
                if($update){
                    $jsondata['success'] = true;
                    $jsondata['token'] = $token2;
                    echo json_encode($jsondata);
                    exit();
                }else{
                    $jsondata['success'] = false;
                    $jsondata['error'] = 3;
                    echo json_encode($jsondata);
                    exit();
                }
            }else{
                $jsondata['success'] = false;
                $jsondata['error'] = 2;                
                echo json_encode($jsondata);
                exit();
            }
        }else{
            $jsondata['success'] = false;
            $jsondata['error'] = 1;
            echo json_encode($jsondata);
            exit();
        }
    }

    function search_email(){
        $email = $_POST['email'];

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/login/model/model/';
        $mail = loadModel($path_model, "login_model", "search_email_recover", $email);

        if($mail == "error"){
            $jsondata['success'] = false;
            $jsondata['error'] = 1;
            echo json_encode($jsondata);
            exit();
        }else{
            if($mail[0]['confirmed'] == false){
                $jsondata['success'] = false;
                $jsondata['error'] = 2;
                echo json_encode($jsondata);
                exit();
            }else{

                $token = "Cha" . md5(uniqid(rand(), true));
                $arrArgument = array(
                            'token' => $token,
                            'email' => $email
                        );
                $update = loadModel($path_model, "login_model", "update_token", $arrArgument);
                sendtoken($arrArgument, "modificacion");
                $jsondata['success'] = true;
                echo json_encode($jsondata);
                exit();
            }
        }
    }

    function update_password(){

        $token = $_POST['token'];
        $password = password_hash($_POST['password'], PASSWORD_BCRYPT);

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/login/model/model/';

        $token_user = loadModel($path_model, "login_model", "search_token", $token);

        if($token_user){
             $user["exist"] = $token_user[0]["COUNT(*)"];
             if ($user["exist"] == 1){
                $data = array(
                    'token' => $token,
                    'password' => $password
                );
                $confirm = loadModel($path_model, "login_model", "update_password", $data);
                
                if ($confirm == true){
                    $jsondata['success'] = true;
                    echo json_encode($jsondata);
                    exit();
                }else{
                    $jsondata['success'] = false;
                    $jsondata['error'] = 1;
                    echo json_encode($jsondata);
                    exit();
                }
             }else{
                $jsondata['success'] = false;
                $jsondata['error'] = 2;
                echo json_encode($jsondata);
                exit();
             }

        }
    }

    function confirm_email(){

        if (substr($_GET['aux'], 0, 3) == "Ver") {

            $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/login/model/model/';

            try {
                $token = loadModel($path_model, "login_model", "search_token", $_GET['aux']);
            } catch (Exception $e) {
                $token = false;
            }

            if($token){
                $user["exist"] = $token[0]["COUNT(*)"];
                if ($user["exist"] == 1){
                    $confirm = loadModel($path_model, "login_model", "confirm_email", $_GET['aux']);
                }
                if ($confirm == true){
                    $jsondata['success'] = true;
                    echo json_encode($jsondata);
                    exit();
                }else{
                    $jsondata['success'] = false;
                    $jsondata['error'] = 3;
                    echo json_encode($jsondata);
                    exit();
                }
            }else{
                $jsondata['success'] = false;
                $jsondata['error'] = 2;
                echo json_encode($jsondata);
                exit();
            }
        }else{
            $jsondata['success'] = false;
            $jsondata['error'] = 1;
            echo json_encode($jsondata);
            exit();
        }
    }
    function update_profile(){

        $user = array(
            'user' => $_POST['user'],
            'name' => $_POST['real_name'],
            'surname' => $_POST['real_surname'],
            'birthday' => $_POST['birthday'],
            'country' => $_POST['country'],
            'province' => $_POST['province'],
            'town' => $_POST['town'],
            'photo' => $_POST['photo']
                        );

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/login/model/model/';

        $confirm = loadModel($path_model, "login_model", "update_profile", $user);

        if($confirm == true){
            $jsondata['success'] = true;
            echo json_encode($jsondata);
            exit();
        }else{
            $jsondata['success'] = false;
            echo json_encode($jsondata);
        }
    }

    function pull_user(){
        
        $token = $_POST['token'];
        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/login/model/model/';
        $user = loadModel($path_model, "login_model", "pull_user", $token);
        echo json_encode($user);
        exit();

    }

    function load_country(){

            $json = array();
            $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/login/model/model/';
            $url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';

            try {
                $json = loadModel($path_model, "login_model", "obtain_countries", $url);
            } catch (Exception $e) {
                $json = false;
            }

            if ($json) {
                echo $json;
                exit;
            } else {
                $json = "error";
                echo $json;
                exit;
            }
    }

    function load_provinces(){
            $jsondata = array();
            $json = array();
            $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/login/model/model/';
            try {
                $json = loadModel($path_model, "login_model", "obtain_provinces");
            } catch (Exception $e) {
                $json = false;
            }

            if ($json) {
                $jsondata["provincias"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["provincias"] = "error";
                echo json_encode($jsondata);
                exit;
            }
    }

        function load_towns() {
        if (isset($_POST['idPoblac'])) {
            $jsondata = array();
            $json = array();

            $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/login/model/model/';

            try {
                $json = loadModel($path_model, "login_model", "obtain_towns", $_POST['idPoblac']);
            } catch (Exception $e) {
                $json = false;
            }

            if ($json) {
                $jsondata["poblaciones"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["poblaciones"] = $_POST['idPoblac'];
                echo json_encode($jsondata);
                exit;
            }
        }
    }

    function personal_comments(){
        $token = $_POST['token'];
        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/login/model/model/';
        $user = loadModel($path_model, "login_model", "pull_user", $token);

        $info = $user[0]['name_user'];
        
        $comments = loadModel($path_model, "login_model", "personal_comments", $info);

        echo json_encode($comments);
        exit();
    }

    function personal_likes(){
        $token = $_POST['token'];
        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/login/model/model/';
        $user = loadModel($path_model, "login_model", "pull_user", $token);

        $info = $user[0]['name_user'];
        
        $likes = loadModel($path_model, "login_model", "personal_likes", $info);

        echo json_encode($likes);
        exit();
    }

    function personal_ranks(){
        $token = $_POST['token'];
        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/login/model/model/';
        $user = loadModel($path_model, "login_model", "pull_user", $token);

        $info = $user[0]['name_user'];
        
        $ranks = loadModel($path_model, "login_model", "personal_ranks", $info);

        echo json_encode($ranks);
        exit();
    }
}