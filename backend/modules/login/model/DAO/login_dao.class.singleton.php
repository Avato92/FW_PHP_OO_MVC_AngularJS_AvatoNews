<?php

class login_DAO {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function search_social_network_user_DAO($db, $user){
    	$id = $user['id'];
    	$sql = "SELECT COUNT(*) FROM users WHERE id = '$id'";

    	return $db->listar($db, $sql);
    }
    public function insert_user_DAO($db, $user){
    	$name = $user['name'];
    	$email = $user['email'];
    	$id = $user['id'];
    	$photo = $user['photo'];
        $token = $user['token'];

    	$sql = "INSERT INTO users (id, name_user, email, photo, sign_in, confirmed, type_user, token2) VALUES('$id', '$name', '$email', '$photo', now(), true, 'reader', '$token')";

    	return $db->ejecutar($sql);
    }
    public function select_user_DAO($db, $id){
    	$sql = "SELECT token2 FROM users WHERE id = '$id'";

    	return $db->listar($db, $sql);
    }
    public function search_user_DAO($db, $user){
        $sql = "SELECT COUNT(*) FROM users WHERE name_user = '$user' AND id IS NULL";

        return $db->listar($db, $sql);
    }
    public function insert_user_manual_DAO($db, $user){
        $name = $user['name'];
        $email = $user['email'];
        $password = $user['password'];
        $photo = $user['photo'];
        $token = $user['token'];
        $token2 = $user['token2'];

        $sql = "INSERT INTO users (name_user, email, password_user, photo, sign_in, token, type_user, confirmed, token2) VALUES ('$name', '$email', '$password', '$photo', now(), '$token', 'reader', false, '$token2')";

        return $db->ejecutar($sql);
    }
    public function search_email_DAO($db, $email){
        $sql = "SELECT COUNT(*) FROM users WHERE email = '$email' AND id IS NULL";

        return $db->listar($db, $sql);
    }
    public function verify_password_DAO($db, $user){
        $sql = "SELECT password_user FROM users WHERE name_user = '$user'";

        return $db->listar($db, $sql);
    }
    public function select_user_manual_DAO($db, $user){
        $sql = "SELECT * FROM users WHERE name_user = '$user'";

        return $db->listar($db, $sql);
    }
    public function search_email_recover_DAO($db, $email){
        $sql = "SELECT email, confirmed FROM users WHERE email = '$email' AND id IS NULL";

        return $db->listar($db, $sql);
    }
    public function update_token_DAO($db, $data){
        $email = $data['email'];
        $token = $data['token'];

        $sql = "UPDATE users SET token = '$token' WHERE email = '$email' AND id IS NULL";

        return $db->ejecutar($sql);
    }
    public function search_token_DAO($db, $token){
        $sql = "SELECT COUNT(*) FROM users WHERE token = '$token'";

        return $db->listar($db, $sql);
    }
    public function update_password_DAO($db, $data){
        $token = $data['token'];
        $password = $data['password'];

        $sql = "UPDATE users SET password_user = '$password' WHERE token = '$token'";

        return $db->ejecutar($sql);
    }
    public function confirm_email_DAO($db, $token){
        $sql = "UPDATE users SET confirmed = true WHERE token = '$token'";

        return $db->ejecutar($sql);
    }
    public function update_profile_DAO($db, $data){
        $user = $data['user'];
        $real_name = $data['name'];
        $real_surname = $data['surname'];
        $birthday = $data['birthday'];
        $photo = $data['photo'];
        $country = $data['country'];
        $province = $data['province'];
        $town = $data['town'];

            $sql = "UPDATE users SET original_name = '$real_name', surname = '$real_surname', date_birthday = '$birthday', country = '$country', photo = '$photo', province = '$province', town = '$town' WHERE name_user = '$user'";

            return $db->ejecutar($sql);
    }
    public function generate_token_DAO($db, $user){
        $token = $user['token'];
        $id = $user['id'];

        $sql = "UPDATE users SET token2 = '$token' WHERE id = '$id'";

        return $db->ejecutar($sql);
    }
    public function pull_user_DAO($db, $token){
      $sql = "SELECT * FROM users WHERE token2 = '$token'";
      
      return $db->listar($db, $sql);  
    }
    public function generate_token_manual_DAO($db, $person){
        $user = $person['name'];
        $token = $person['token'];

        $sql = "UPDATE users SET token2 = '$token' WHERE name_user = '$user'";

        return $db->ejecutar($sql);
    }

    public function obtain_countries_DAO($url){

          $ch = curl_init();
          curl_setopt ($ch, CURLOPT_URL, $url);
          curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
          $file_contents = curl_exec($ch);

          $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          curl_close($ch);
          $accepted_response = array(200, 301, 302);
          if(!in_array($httpcode, $accepted_response)){
            return FALSE;
          }else{
            return ($file_contents) ? $file_contents : FALSE;
          }
    }
    public function obtain_provinces_DAO() {
        $json = array();
        $tmp = array();

        $provincias = simplexml_load_file( RESOURCES_PATH . "provinciasypoblaciones.xml");
        $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
        for ($i = 0; $i < count($result); $i+=2) {
            $e = $i + 1;
            $provincia = $result[$e];
            $tmp = array(
                'id' => (string) $result[$i], 'nombre' => (string) $provincia
            );
            array_push($json, $tmp);
        }
        return $json;
    }
    public function obtain_towns_DAO($arrArgument) {
        $json = array();
        $tmp = array();

        $filter = (string) $arrArgument;
        $xml = simplexml_load_file(RESOURCES_PATH . 'provinciasypoblaciones.xml');
        $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");

        for ($i = 0; $i < count($result[0]); $i++) {
            $tmp = array(
                'poblacion' => (string) $result[0]->localidad[$i]
            );
            array_push($json, $tmp);
        }
        return $json;
    }
    public function personal_comments_DAO($db, $user){
        $sql = "SELECT * FROM comments WHERE user = '$user'";

        return $db->listar($db, $sql);
    }
    public function personal_likes_DAO($db, $user){
        $sql = "SELECT * FROM likes WHERE user = '$user'";

        return $db->listar($db, $sql);
    }
    public function personal_ranks_DAO($db, $user){
        $sql = "SELECT * FROM rank WHERE user = '$user'";

        return $db->listar($db, $sql);
    }
}