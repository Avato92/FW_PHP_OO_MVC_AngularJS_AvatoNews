<?php

class login_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = login_bll::getInstance();
    }
    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
	public function search_social_network_user($user){
    	return $this->bll->search_social_network_user_BLL($user);
    }
    public function insert_user($user){
        return $this->bll->insert_user_BLL($user);
    }
    public function select_user($id){
        return $this->bll->select_user_BLL($id);
    }
    public function search_user($user){
        return $this->bll->search_user_BLL($user);
    }
    public function insert_user_manual($user){
        return $this->bll->insert_user_manual_BLL($user);
    }
    public function search_email($email){
        return $this->bll->search_email_BLL($email);
    }
    public function verify_password($user){
        return $this->bll->verify_password_BLL($user);
    }
    public function select_user_manual($user){
        return $this->bll->select_user_manual_BLL($user);
    }
    public function search_email_recover($email){
        return $this->bll->search_email_recover_BLL($email);
    }
    public function update_token($data){
        return $this->bll->update_token_BLL($data);
    }
    public function search_token($token){
        return $this->bll->search_token_BLL($token);
    }
    public function update_password($data){
        return $this->bll->update_password_BLL($data);
    }
    public function confirm_email($token){
        return $this->bll->confirm_email_BLL($token);
    }
    public function update_profile($data){
        return $this->bll->update_profile_BLL($data);
    }
    public function generate_token($user){
        return $this->bll->generate_token_BLL($user);
    }
    public function pull_user($token){
        return $this->bll->pull_user_BLL($token);
    }
    public function generate_token_manual($person){
        return $this->bll->generate_token_manual_BLL($person);
    }
    public function obtain_countries($url) {
            return $this->bll->obtain_countries_BLL($url);
        }
    public function obtain_provinces() {
            return $this->bll->obtain_provinces_BLL();
        }
    public function obtain_towns($arrArgument) {
            return $this->bll->obtain_towns_BLL($arrArgument);
        }
    public function personal_comments($user){
        return $this->bll->personal_comments_BLL($user);
    }
    public function personal_likes($user){
        return $this->bll->personal_likes_BLL($user);
    }
    public function personal_ranks($user){
        return $this->bll->personal_ranks_BLL($user);
    }
}