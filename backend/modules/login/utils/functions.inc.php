<?php


    function get_gravatar($email, $s = 80, $d = 'wavatar', $r = 'g', $img = false, $atts = array()) {
        $email = trim($email);
        $email = strtolower($email);
        $email_hash = md5($email);

        $url = "https://www.gravatar.com/avatar/" . $email_hash;
        $url .= md5(strtolower(trim($email)));
        $url .= "?s=$s&d=$d&r=$r";
        if ($img) {
            $url = '<img src="' . $url . '"';
            foreach ($atts as $key => $val)
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }

    function sendtoken($data, $type) {
        $mail = array(
            'type' => $type,
            'token' => $data['token'],
            'inputEmail' => $data['email']
        );
        try {
            enviar_email($mail);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }