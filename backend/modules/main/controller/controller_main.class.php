<?php

class controller_main {
    function __construct() {
        $_SESSION['module'] = "main";
    }

  function begin() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/main/view/', 'category.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

  function number_pages(){
        $json = array();

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/main/model/model/';
        $json = loadModel($path_model, "main_model", "obtain_number_pages");

        if($json){
          $jsondata["num_news"] = $json[0]['COUNT(*)'];
          echo json_encode($jsondata);
          exit;
        }else{
          $json = "error";
          echo json_encode($json);
          exit;
        }
    }

  function page(){
    if(isset($_POST['page'])){
      $news_page = 3;
      $page = $_POST['page'];
      $position = (($page - 1) * $news_page);
      $model_path = $_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/main/model/model/';
        $arrArgument = array(
            "position" => $position,
            "limit" => $news_page);
        try {
          $resultado = loadModel($model_path, "main_model", "select_limit_news", $arrArgument);
        } catch (Exception $e) {
          echo json_encode("error1");
          exit();
        }
        if ($resultado) {
          echo json_encode($resultado);
          exit();
        } else {
          $resultado = "error";
          echo json_enode($resultado);
          exit();
        }
      }
    }

  function list_categories(){

     $model_path = $_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/main/model/model/';
      //$model_path = $_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/main/model/model/';
      if(isset($_POST['key'])){
        $key = $_POST['key'];
        try{
          $resultado = loadModel($model_path, "main_model", "select_categories_2", $key);
        } catch (Exception $e){
          echo json_encode("error1");
          exit();
        }
        if ($resultado) {
          echo json_encode($resultado);
          exit();
      } else {
          $resultado = "error";
          echo json_enode($resultado);
          exit();
      }
      }
      try {
        $resultado = loadModel($model_path, "main_model", "select_categories");
      } catch (Exception $e) {
        echo json_encode("error1");
        exit();
      }
      if ($resultado) {
          echo json_encode($resultado);
          exit();
      } else {
          $resultado = "error";
          echo json_enode($resultado);
          exit();
      }
  }

  function list_subcategories(){

      $model_path = $_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/main/model/model/';
      //$model_path = $_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/main/model/model/';
      if(isset($_POST['category'])){
        $cat = $_POST['category'];
        try {
          $resultado = loadModel($model_path, "main_model", "select_subcategories2", $cat);
          echo json_encode($resultado);
          exit();
        } catch (Exception $e) {
          echo json_encode("error1");
          exit();
        }
        if ($resultado) {
          echo json_encode($resultado);
          exit();
        } else {
          $resultado = "error";
          echo json_enode($resultado);
          exit();
        }
      }
      try {
        $resultado = loadModel($model_path, "main_model", "select_subcategories");
      } catch (Exception $e) {
        echo json_encode("error1");
        exit();
      }
      if ($resultado) {
          echo json_encode($resultado);
          exit();
      } else {
          $resultado = "error";
          echo json_enode($resultado);
          exit();
      }
  }

  function list_specific_categories(){

      $cat = $_POST['category'];
      $sub = $_POST['subcategory'];

            $find_cat = array(
            'cat' => $cat,
            'subcat' => $sub);
      //$model_path = $_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/main/model/model/';
      $model_path = $_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/main/model/model/';
      try {
        $resultado = loadModel($model_path, "main_model", "select_specific_categories", $find_cat);
      } catch (Exception $e) {
        echo json_encode("error1");
        exit();
      }
      if ($resultado) {
          echo json_encode($resultado);
          exit();
      } else {
          $resultado = "error";
          echo json_enode($resultado);
          exit();
      }
  }

  function search_specific_news(){

      $cat =$_POST['category'];
      $sub = $_POST['subcategory'];
      $spec = $_POST['specific_category'];
      $news_page = 3;
      $find_cat = array(
            'cat' => $cat,
            'subcat' => $sub,
            'spec' => $spec,
            'limit' => $news_page);
      $model_path = $_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/main/model/model/';
      try {
        $resultado = loadModel($model_path, "main_model", "select_news", $find_cat);
      } catch (Exception $e) {
        echo json_encode("error1");
        exit();
      }
      if ($resultado) {
          echo json_encode($resultado);
          exit();
      } else {
          $resultado = "error";
          echo json_enode($resultado);
          exit();
      }
  }

  function autocomplete(){
      $key = $_POST['key'];

      $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/main/model/model/';
      $json = loadModel($path_model, "main_model", "autocomplete", $key);

      if($json){
        echo json_encode($json);
        exit;
      }else{
        $json = "error";
        echo json_encode($json);
        exit;
      }
    }

    function list_all_news(){

      $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/modules/main/model/model/';
      $json = loadModel($path_model, "main_model", "pull_all_news");

      echo json_encode($json);
      exit();
    }
}