<?php

class main_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = main_DAO::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function obtain_number_pages_BLL(){
      return $this->dao->obtain_number_pages_DAO($this->db);
    }
    public function select_limit_news_BLL($array){
        return $this->dao->select_limit_news_DAO($this->db, $array);
    }
    public function select_categories_BLL(){
        return $this->dao->select_categories_DAO($this->db);
    }
    public function select_categories_2_BLL($key){
        return $this->dao->select_categories_2_DAO($this->db, $key);
    }
    public function select_subcategories_BLL(){
        return $this->dao->select_subcategories_DAO($this->db);
    }
    public function select_subcategories2_BLL($cat){
        return $this->dao->select_subcategories2_DAO($this->db, $cat);
    }
    public function select_specific_categories_BLL($cat){
        return $this->dao->select_specific_categories_DAO($this->db, $cat);
    }
    public function select_news_BLL($cat){
        return $this->dao->select_news_DAO($this->db, $cat);
    }
    public function autocomplete_BLL($key){
        return $this->dao->autocomplete_DAO($this->db, $key);
    }
    public function pull_all_news_BLL(){
        return $this->dao->pull_all_news_DAO($this->db);
    }
}