<?php

class main_DAO {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function obtain_number_pages_DAO($db){
        $sql = 'SELECT COUNT(*) FROM news';

        return $db->listar($db,$sql);
    }

    public function select_limit_news_DAO($db, $array){
        $sql = 'SELECT * FROM news ORDER BY event_date LIMIT ' . $array['position'] . ',' . $array['limit'];
        return $db->listar($db,$sql);
    }

    public function select_categories_DAO($db){
        $sql = 'SELECT * FROM category';
        return $db->listar($db, $sql);
    }
    public function select_categories_2_DAO($db, $key){
        $sql = "SELECT * FROM category WHERE name LIKE '%$key%'";
        return $db->listar($db, $sql);
    }
    public function select_subcategories_DAO($db){
        $sql = 'SELECT * FROM subcategory';
        return $db->listar($db, $sql);
    }
    public function select_subcategories2_DAO($db, $cat){
        $sql = "SELECT * FROM subcategory WHERE cod_cat='$cat'";
        return $db->listar($db, $sql);
    }
    public function select_specific_categories_DAO($db, $cat){
        $category = $cat['cat'];
        $subcategory = $cat['subcat'];
        $sql = "SELECT * FROM specific_cat WHERE cod_cat='$category' AND cod_sub ='$subcategory'";
        return $db->listar($db, $sql);
    }
    public function select_news_DAO($db, $cat){
        $category = $cat['cat'];
        $subcategory = $cat['subcat'];
        $spec = $cat['spec'];
        $sql = "SELECT * FROM news WHERE main_group = '$category' AND second_group = '$subcategory' AND specific_cat = '$spec'";
        return $db->listar($db,$sql);
    }
    public function autocomplete_DAO($db, $key){
        $sql = "SELECT name FROM category WHERE name LIKE '%$key%'";
        return $db->listar($db,$sql);
    }
    public function pull_all_news_DAO($db){
        $sql = "SELECT * FROM news";
        return $db->listar($db, $sql);
    }
}