<?php

class news_DAO {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_new_DAO($db, $arrArgument) {
        $author = $arrArgument['author'];
        $id_new = $arrArgument['id_new'];
        $headline = $arrArgument['headline'];
        $event_date = $arrArgument['event_date'];
        $publication_date = $arrArgument['publication_date'];
        $allied = $arrArgument['allied'];
        $category = $arrArgument['category'];
        $path = $arrArgument['path'];

        $cat1="";
        $cat2="";
        $cat3="";
        $cat4="";
        $cat5="";

        foreach ($allied as $indice) {
            if ($indice === 'Economía')
                $cat1 = "Economía";
            if ($indice === 'Internacional')
                $cat2 = "Internacional";
            if ($indice === 'Cultura')
                $cat3 = "Cultura";
            if ($indice === 'España')
                $cat4 = "España";
            if ($indice === 'Deportes')
                $cat5 = "Deportes";
        }
        
        $sql = "INSERT INTO news (author, id_new, headline, event_date,"
                . " publication_date, cat1, cat2, cat3, cat4, cat5, category, path)"
                . " VALUES ('$author', '$id_new',"
                . " '$headline', '$event_date', '$publication_date', '$cat1', "
                . " '$cat2', '$cat3', '$cat4', '$cat5', '$category', '$path')";

        return $db->ejecutar($sql);
    }

    public function obtain_categories_DAO($db){
        $sql = 'SELECT * FROM category';

        return $db->listar($db,$sql);
    }

    public function obtain_subcategories_DAO($category, $db){
        $sql = "SELECT * FROM subcategory WHERE cod_cat = '$category'";

        return $db->listar($db, $sql);
    }

    public function obtain_specific_categories_DAO($cat, $db){
        $category = $cat['cat'];
        $subcategory = $cat['subcat'];

        $sql = "SELECT * FROM specific_cat WHERE cod_cat = '$category' AND cod_sub = '$subcategory'";

        return $db->listar($db, $sql);
    }
}
