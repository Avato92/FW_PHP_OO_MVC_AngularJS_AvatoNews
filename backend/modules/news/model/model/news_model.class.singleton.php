<?php

// $path = $_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/';
// define('PATH_ROOT', $path);
// require(PATH_ROOT . "modules/news/model/BLL/news_bll.class.singleton.php");

class news_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = news_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_new($arrArgument) {
        return $this->bll->create_new_BLL($arrArgument);
    }

    public function obtain_categories(){
        return $this->bll->obtain_categories_BLL();
    }

    public function obtain_subcategories($category){
        return $this->bll->obtain_subcategories_BLL($category);
    }

    public Function obtain_specific_categories($arrArgument){
        return $this->bll->obtain_specific_categories_BLL($arrArgument);
    }

}