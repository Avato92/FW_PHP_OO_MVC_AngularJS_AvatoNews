<?php

/* SITE ROOT */

$path = $_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/';
define('SITE_ROOT', $path);

/* SITE PATH */

define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/FW_PHP_OO_AngularJS_AvatoNews/backend/');

/* CSS */

//define('CSS_PATH', SITE_PATH . 'view/css/');

/* JS */

//define('JS_PATH', SITE_PATH . 'view/js/');

/* IMG */

//define('IMG_PATH', SITE_PATH . 'view/img/');

/* MODEL */

define('MODEL_PATH', SITE_ROOT . 'model/');

/*  VIEW */

//define('VIEW_PATH_INC', SITE_ROOT . 'view/inc/');

/* MODULES */

define('MODULES_PATH', SITE_ROOT . 'modules/');

/* RESOURCES */

define('RESOURCES_PATH', SITE_ROOT . 'resources/');

/* MEDIA */

define('MEDIA_ROOT', SITE_ROOT . 'media/');
define('MEDIA_PATH', SITE_PATH . 'media/');

/* UTILS */

define('UTILS_PATH', SITE_ROOT . 'utils/');

/* LIBS */

define('LIBS', SITE_ROOT . 'libs/');

/* CLASSES */

define('CLASSES', SITE_ROOT . 'classes/');

/* MODUL NEWS */

define('UTILS_PATH_NEWS', SITE_ROOT . 'modules/news/utils/');
define('MODEL_NEWS', SITE_ROOT . 'modules/news/model/');
define('DAO_PATH_NEWS', SITE_ROOT . 'modules/news/model/DAO/');
define('BLL_PATH_NEWS', SITE_ROOT . 'modules/news/model/BLL/');
define('MODEL_PATH_NEWS', SITE_ROOT . 'modules/news/model/model/');
// define('JS_PATH_NEWS', SITE_PATH . 'modules/news/view/js/');

/* MODUL HOME */

define('MODEL_MAIN', SITE_ROOT . 'modules/main/model/');
define('DAO_PATH_MAIN', SITE_ROOT . 'modules/main/model/DAO/');
define('BLL_PATH_MAIN', SITE_ROOT . 'modules/main/model/BLL/');
define('MODEL_PATH_MAIN', SITE_ROOT . 'modules/main/model/model/');
// define('JS_PATH_MAIN', SITE_PATH . 'modules/main/view/js/');

/* MODUL CONTACT */

// define('JS_PATH_CONTACT', SITE_PATH . 'modules/contact/view/js/');
// define('CSS_PATH_CONTACT', SITE_PATH . 'modules/contact/view/css/');
define('LIB_PATH_CONTACT', SITE_PATH . 'modules/contact/view/lib/');
define('IMG_PATH_CONTACT', SITE_PATH . 'modules/contact/view/img/');
define('VIEW_PATH_CONTACT', 'modules/contact/view/');

/* MODUL INTERNATIONAL */

define('MODEL_INTERNATIONAL', SITE_ROOT . 'modules/international/model/');
define('DAO_PATH_INTERNATIONAL', SITE_ROOT . 'modules/international/model/DAO/');
define('BLL_PATH_INTERNATIONAL', SITE_ROOT . 'modules/international/model/BLL/');
define('MODEL_PATH_INTERNATIONAL', SITE_ROOT . 'modules/international/model/model/');
// define('JS_PATH_INTERNATIONAL', SITE_PATH . 'modules/international/view/js/');
// define('CSS_PATH_INTERNATIONAL', SITE_PATH . 'modules/international/view/css/');

/* MODUL LOGIN */

define('MODEL_LOGIN', SITE_ROOT . 'modules/login/model/');
define('DAO_PATH_LOGIN', MODEL_LOGIN . 'DAO/');
define('BLL_PATH_LOGIN', MODEL_LOGIN . 'BLL/');
define('MODEL_PATH_LOGIN', MODEL_LOGIN . 'model/');
// define('JS_PATH_LOGIN', SITE_PATH . 'modules/login/view/js/');
define('UTILS_PATH_LOGIN', SITE_ROOT . 'modules/login/utils/');

/* AMIGABLES */

define('URL_AMIGABLES', TRUE);