<?php
header('Content-Type: text/html; charset=UTF-8');

    function enviar_email($arr) {
        $html = '';
        $subject = '';
        $body = '';
        $ruta = '';
        $return = '';
        $url_web = 'http://FW_PHP_OO_AngularJS_AvatoNews/';
        
        switch ($arr['type']) {
            case 'alta':
                $subject = 'Tu Alta a AvatoNews';
                $ruta = "<a href='" . "http://127.0.0.1/FW_PHP_OO_AngularJS_AvatoNews/#/login/confirm_email/" . $arr['token'] . "'>aqu&iacute;</a>";
                $body = 'Gracias por unirte a nuestra aplicaci&oacute;n<br> Para finalizar el registro, pulsa ' . $ruta;
                break;
    
            case 'modificacion':
                $subject = 'Tu Nuevo Password a AvatoNews';
                $ruta = '<a href="' . "http://127.0.0.1/FW_PHP_OO_AngularJS_AvatoNews/#/login/change_password/" . $arr['token'] . '">aqu&iacute;</a>';
                $body = 'Para cambiar tu password pulsa ' . $ruta;
                break;
                
            case 'contact':
                $subject = 'Tu Petición a AvatoNews ha sido enviada';
                $ruta = '<a href=' . $url_web . 'main' . '>aqu&iacute;</a>';
                $body = 'Para visitar nuestra web, pulsa ' . $ruta;
                break;
    
            case 'admin':
                $subject = $arr['inputSubject'];
                $body = 'Nombre: ' . $arr['inputName']. '<br>' .
                'email: ' . $arr['inputEmail']. '<br>' .
                'motivo: ' . $arr['inputSubject']. '<br>' .
                'Mensaje: ' . $arr['inputMessage'];
                break;
        }
        
        $html .= "<html>";
        $html .= "<head>";
        $html .= "<meta charset='utf-8' />
        <style>
            * {
                margin: 0;
                padding: 0;
                text-align: center;
              }

            body {
                margin: 0 auto;
                width: 600px;
                height: 300px;
            }
              
            header {
                padding: 20px;
                background-color: blue;
                color: white;
                padding-left: 20px;
                font-size: 25px;
            }
               
            section {
                padding-top: 50px;
                padding-left: 50px;
                margin-top: 3px;
                margin-bottom: 3px;
                height: 100px;
                background-color: ghostwhite;
              }
              
             footer {
                padding: 5px;
                padding-left: 20px;
                background-color: blue;
                color: white;
              }
        </style>";
         $html .= "</head>";
        $html .= "<body>";
        $html .= "<header>";
            $html .= "<p>" . $subject . "</p>";
        $html .= "</header>";
        $html .= "<section>";
            $html .= $body;
        $html .= "</section>";
        $html .= "<footer>";
            $html .= "<p>Enviado por AvatoNews</p>";
        $html .= "</footer>";
    $html .= "</body>";
    $html .= "</html>";
        try{

            if ($arr['type'] === 'admin')
                $address = 'avato92@gmail.com';
            else
                $address = $arr['inputEmail'];
                $result = send_mailgun('avato92@gmail.com', $address, $subject, $html);    
        } catch (Exception $e) {
			$return = 0;
		}
        return $result;
    }
    
    function send_mailgun($from, $to, $subject, $html){
        	$config = array();
        	$config['api_key'] = "key-80911eaf590f8b09c2b0decbb009c702";
        	$config['api_url'] = "https://api.mailgun.net/v3/sandbox5e7f6fac18914a2a8e0dfb51fef13b28.mailgun.org/messages";
    
        	$message = array();
        	$message['from'] = $from;
        	$message['to'] = $to;
        	$message['h:Reply-To'] = "avato92@gmail.com";
        	$message['subject'] = $subject;
        	$message['html'] = $html;
         
        	$ch = curl_init();
        	curl_setopt($ch, CURLOPT_URL, $config['api_url']);
        	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        	curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        	curl_setopt($ch, CURLOPT_POST, true); 
        	curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
        	$result = curl_exec($ch);
        	curl_close($ch);
        	return $result;
        }