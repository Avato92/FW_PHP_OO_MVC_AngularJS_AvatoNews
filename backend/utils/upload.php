<?php

function upload_files() {
    $error = "";
    $copiarFichero = false;
    $extensiones = array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'txt');

    if(!isset($_FILES)) {
        $error .=  'No existe $_FILES <br>';
    }
    if(!isset($_FILES['file'])) {
        $error .=  'No existe $_FILES[file] <br>';
    }

    $imagen = $_FILES['file']['tmp_name'];
    $nom_fitxer= $_FILES['file']['name'];
    $mida_fitxer=$_FILES['file']['size'];
    $tipus_fitxer=$_FILES['file']['type'];
    $error_fitxer=$_FILES['file']['error'];

    if ($error_fitxer>0) {
        switch ($error_fitxer){
            case 1:
                    $error .=  'El archivo es demasiado pesado <br>';
                    break;
            case 2:
                    $error .=  'El archivo es demasiado grande <br>';
                    break;
            case 3: 
                    $error .=  'Archivo subido parcialmente <br>';
                    break;
            case 4: 
                    $error .=  'No has subido ningun archivo <br>';
                    break;
        }
    }

    if ($_FILES['file']['size'] > 55000 ){
        $error .=  "Archivo demasiado grande <br>";
    }

    if ($_FILES['file']['name'] !== "") {
        @$extension = strtolower(end(explode('.', $_FILES['file']['name'])));
        if( ! in_array($extension, $extensiones)) {
            $error .=  'Sólo se permite subir archivos con estas extensiones: ' . implode(', ', $extensiones).' <br>';
        }
        if (!@getimagesize($_FILES['file']['tmp_name'])){
            $error .=  "Archivo invalido... <br>";
        }
        list($width, $height, $type, $attr) = @getimagesize($_FILES['file']['tmp_name']);
        if ($width > 150 || $height > 150){
            $error .=   "Tamaño excedido. Por favor suba imagenes maximo de 100x100 px<br>";
        }
    }
    $upfile = $_SERVER['DOCUMENT_ROOT'].'/FW_PHP_OO_AngularJS_AvatoNews/backend/media/'.$_FILES['file']['name'];
    if (is_uploaded_file($_FILES['file']['tmp_name'])){
        if (is_file($_FILES['file']['tmp_name'])) {
            $nombreFichero = $_FILES['file']['name'];
            $_SESSION['nombreFichero'] = $nombreFichero;
            $copiarFichero = true;
            $upfile = $_SERVER['DOCUMENT_ROOT']."/FW_PHP_OO_AngularJS_AvatoNews/backend/media/".$nombreFichero;
        }else{
                $error .=   "Archivo no válido...";
        }
    }

    $i=0;
    if ($error == "") {
        if ($copiarFichero) {
            if (!move_uploaded_file($_FILES['file']['tmp_name'], $upfile)) {
                $error .= "<p>Error al subir el archivo.</p>";
                return $return=array('result'=>false,'error'=>$error,'data'=>"");
            }
            $upfile = '/FW_PHP_OO_AngularJS_AvatoNews/backend/media/'.$nombreFichero;
            return $return=array('result'=>true , 'error'=>$error,'data'=>$upfile);
        }
    }else{
        return $return=array('result'=>false,'error'=>$error,'data'=>"");
    }
}

function remove_files(){
    $name = $_POST['filename'];
    if(file_exists($_SERVER['DOCUMENT_ROOT'].'/FW_PHP_OO_AngularJS_AvatoNews/backend/media/'.$_SESSION['nombreFichero'])){
        unlink($_SERVER['DOCUMENT_ROOT'].'/FW_PHP_OO_AngularJS_AvatoNews/backend/media/'.$_SESSION['nombreFichero']);
        return true;
    }else{
        return false;
    }
}