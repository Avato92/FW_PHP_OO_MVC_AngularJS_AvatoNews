var AvatoNews = angular.module('AvatoNews',['ngRoute','ui.bootstrap']);
AvatoNews.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider

                // Contact

                .when("/contact", {
                    templateUrl: "frontend/modules/contact/view/contact.view.html", 
                    controller: "contactCtrl"
                })

                //Main

                .when("/categories",{
                    templateUrl: "frontend/modules/main/view/main.view.html",
                    controller: "mainCtrl",
                    resolve: {
                        categories: function(services){
                            return services.get('main', 'list_categories');
                        }
                    }
                })

                //Google Maps

                .when("/international",{
                    templateUrl: "frontend/modules/international/view/map.view.html",
                    controller: "internationalCtrl",
                    resolve: {
                        news: function(services){
                            return services.get('main','list_all_news');
                        }
                    }
                })

                //Details Google Maps

                .when("/international/:id",{
                    templateUrl: "frontend/modules/international/view/details_new.view.html",
                    controller: "detailsCtrl",
                    resolve: {
                        details: function (services, $route) {
                            return services.get('international', 'search_details', $route.current.params.id);
                        },
                        likes: function (services, $route) {
                            return services.get('international', 'number_likes', $route.current.params.id);
                        },
                        comments: function(services, $route) {
                            return services.get('international', 'comments', $route.current.params.id);
                        },
                        rank: function(services, $route){
                            return services.get('international', 'rank', $route.current.params.id);
                        }
                    }
                })

                //Login

                .when("/login",{
                    templateUrl: "frontend/modules/login/view/signin.view.html",
                    controller: "loginCtrl"
                })

                //Confirmar email

                .when("/login/confirm_email/:token",{
                    templateUrl: "frontend/modules/main/view/main.view.html",
                    controller: "confirmCtrl"
                })

                //Registrarse

                .when("/register",{
                    templateUrl: "frontend/modules/login/view/register.view.html",
                    controller: "registerCtrl"
                })

                //Solicitud de cambio de contraseña

                .when("/recover",{
                    templateUrl: "frontend/modules/login/view/recover.view.html",
                    controller: "recoverCtrl"
                })

                //Pantalla de modificación de contraseña

                .when("/login/change_password/:token",{
                    templateUrl: "frontend/modules/login/view/change_password.view.html",
                    controller: "changePassCtrl"
                })

                //Perfil

                .when("/login/profile",{
                    templateUrl: "frontend/modules/login/view/profile.view.html",
                    controller: "profileCtrl",
                    resolve:{
                        user: function(services, localStorage){
                            var person = localStorage.retrieve();
                            var info = JSON.stringify(person);

                            if(person){
                                return services.post('login', 'pull_user', info);
                            }else{
                                return false;
                            }
                        },
                        comment: function(services, localStorage){
                            var person = localStorage.retrieve();
                            var info = JSON.stringify(person);

                            if(person){
                                return services.post('login', 'personal_comments', info);
                            }else{
                                return false;
                            }
                        },
                        likes: function(services, localStorage){
                            var person = localStorage.retrieve();
                            var info = JSON.stringify(person);

                            if(person){
                                return services.post('login', 'personal_likes', info);
                            }else{
                                return false;
                            }
                        },
                        ranks: function(services, localStorage){
                            var person = localStorage.retrieve();
                              var info = JSON.stringify(person);

                            if(person){
                                return services.post('login', 'personal_ranks', info);
                            }else{
                                return false;
                            }
                        }
                    }
                })

                 .otherwise("/", {
                    templateUrl: "frontend/modules/main/view/main.view.html",
                    controller: "mainCtrl"
                });
    }]);
