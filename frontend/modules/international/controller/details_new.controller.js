AvatoNews.controller('detailsCtrl', function($scope, details, localStorage, comments, services, $route, CommonService, $location, likes, rank){
    var user = localStorage.retrieve();
    $scope.sec_comments = true;
    var new_id = $route.current.params.id;
    $scope.number_likes = likes;
    $scope.rank_stars = rank;



    if (comments != "error"){
        $scope.comments = comments;
    }else{
        $scope.sec_comments = false;
        $scope.empty_comments = true;
        $scope.no_comments = "No hay comentarios se el primero en comentar.";
    }
    if(user.token){
        $scope.area_comments = true;
        $scope.rank = true;
    }else{
        $scope.area_comments = false;
        $scope.rank = false;  
    }

    $scope.rank = function(value){
        var data = {user: user, id : new_id, rank : value};
        var info = JSON.stringify(data);
        services.post('international', 'rank_news', info).then(function(response){
                $route.reload();
        });
    }


    $scope.sent_comment = function(){

       var comment = $scope.new_comment;

       var data = {comment : comment, user: user, id_new : new_id};
       var info = JSON.stringify(data);

       services.post('international', 'new_comment', info).then(function(response){
           if(response.success == true){
            $route.reload();
           }else{
            CommonService.banner("Ha habido un error, no se ha podido registrar el comentario", "Add");
            $location.path("/");
           }
       });
    }

    $scope.like_this = function(){

        var data = {user : user, id_new : new_id};
        var info = JSON.stringify(data);

        services.post('international', 'like_this', info).then(function(response){
            console.log(response);
            if(response.success == true){
                CommonService.banner("Te gusta!", "Add");
            }else{
                if(response.error == 1){
                    CommonService.banner("Usuario no encontrado, vuelvete a loguear y si el error persiste contacta con un administrador", "Add");
                    $location.path("/");
                }else if(response.error == 2){
                    $scope.liked = true;
                }
            }
        });
    }

	var content = document.getElementById("content");
    var div_product = document.createElement("div");
    var parrafo = document.createElement("p");

    var author = document.createElement("div");
    author.innerHTML = "Autor: ";
    author.innerHTML += details[0].author;

    var event_date = document.createElement("div");
    event_date.innerHTML = "Fecha del suceso: ";
    event_date.innerHTML += details[0].event_date;

    var publication_date = document.createElement("div");
    publication_date.innerHTML = "Fecha de publicación: ";
    publication_date.innerHTML += details[0].publication_date;


    var headline = document.createElement("div");
    headline.innerHTML = "Titular: ";
    headline.innerHTML += details[0].headline;

    var category = document.createElement("div");
    category.innerHTML = "Categoria: ";
    category.innerHTML += details[0].category;

    var path = document.createElement("div");
    path.innerHTML = "Ruta de la noticia: ";
    path.innerHTML += details[0].path;

    div_product.appendChild(parrafo);
    parrafo.appendChild(author);
    parrafo.appendChild(headline);
    parrafo.appendChild(event_date);
    parrafo.appendChild(publication_date);
    parrafo.appendChild(category);
    parrafo.appendChild(path);
    content.appendChild(div_product);


});