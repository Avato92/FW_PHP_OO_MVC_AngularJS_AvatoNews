AvatoNews.factory("functions_map",['$rootScope',
	function ($rootScope){
		var map, places, infoWindow;
      	var markers = [];
      	var autocomplete;
      	var countryRestrict = {'country': 'es'};
      	var MARKER_PATH = 'https://maps.gstatic.com/intl/en_us/mapfiles/marker_green';
      	var hostnameRegexp = new RegExp('^https?://.+?/');

      	var countries = {
	        'au': {
	          center: {lat: -25.3, lng: 133.8},
	          zoom: 4
	        },
	        'br': {
	          center: {lat: -14.2, lng: -51.9},
	          zoom: 3
	        },
	        'ca': {
	          center: {lat: 62, lng: -110.0},
	          zoom: 3
	        },
	        'fr': {
	          center: {lat: 46.2, lng: 2.2},
	          zoom: 5
	        },
	        'de': {
	          center: {lat: 51.2, lng: 10.4},
	          zoom: 5
	        },
	        'mx': {
	          center: {lat: 23.6, lng: -102.5},
	          zoom: 4
	        },
	        'nz': {
	          center: {lat: -40.9, lng: 174.9},
	          zoom: 5
	        },
	        'it': {
	          center: {lat: 41.9, lng: 12.6},
	          zoom: 5
	        },
	        'za': {
	          center: {lat: -30.6, lng: 22.9},
	          zoom: 5
	        },
	        'es': {
	          center: {lat: 40.5, lng: -3.7},
	          zoom: 5
	        },
	        'pt': {
	          center: {lat: 39.4, lng: -8.2},
	          zoom: 6
	        },
	        'us': {
	          center: {lat: 37.1, lng: -95.7},
	          zoom: 3
	        },
	        'uk': {
	          center: {lat: 54.8, lng: -4.6},
	          zoom: 5
	        }
	      };
		var service = {};
		service.load_map = load_map;
		service.onPlaceChanged = onPlaceChanged;
		service.setAutocompleteCountry = setAutocompleteCountry;
		service.clearMarkers = clearMarkers;
		service.dropMarker = dropMarker;
		service.clearResults = clearResults;
		service.infoWindow = infoWindow;
		service.addResult = addResult;

		return service;

			function load_map(news){
				$rootScope.news = news;
				 map = new google.maps.Map(document.getElementById('map'), {
		         zoom: countries['es'].zoom,
		         center: countries['es'].center,
		         mapTypeControl: false,
		         panControl: false,
		         zoomControl: false,
		         streetViewControl: false
		        });

		        infoWindow = new google.maps.InfoWindow({
		         content: document.getElementById('info-content')
		        });

		        autocomplete = new google.maps.places.Autocomplete((
		                document.getElementById('autocomplete')), {
		              		types: ['(cities)'],
		              		componentRestrictions: countryRestrict
		            	});
		        places = new google.maps.places.PlacesService(map);

		        autocomplete.addListener('place_changed', onPlaceChanged);

		        document.getElementById('country').addEventListener(
		            'change', setAutocompleteCountry);
					}	

		function onPlaceChanged() {
	        var place = autocomplete.getPlace();
	        if(place.geometry){
	            map.panTo(place.geometry.location);
	            map.setZoom(15);
	            var coordenadas = {
	              bounds:  map.getBounds().b.f,
	              bounds2: map.getBounds().b.b,
	              bounds3: map.getBounds().f.b,
	              bounds4: map.getBounds().f.f
	            };
	    	}
	    	var i = 0;
	    	angular.forEach($rootScope.news, function(){
	    		var lat = $rootScope.news[i].latitud;
	    		var lon = $rootScope.news[i].longitud;

	    		if(lat <= coordenadas.bounds4){
	    			if(lat >= coordenadas.bounds3){
	    				if(lon <= coordenadas.bounds){
	    					if(lon >= coordenadas.bounds2){
	    						var markerLetter = String.fromCharCode('A'.charCodeAt(0) + i);
                    			var markerIcon = MARKER_PATH + markerLetter + '.png';

			                    markers[i] = new google.maps.Marker({
			                    position: new google.maps.LatLng(lat, lon),
			                    animation: google.maps.Animation.DROP,
			                    icon: markerIcon
			                    });
			                    markers[i].placeResult = $rootScope.news[i];
                    			google.maps.event.addListener(markers[i], 'click', showInfoWindow);
                    			setTimeout(dropMarker(i), i * 100);
                    			addResult($rootScope.news[i], i);
	    					}
	    				}
	    			}
	    		} 
	    		i++;
	    	});
	    }

	    function setAutocompleteCountry() {
	        var country = document.getElementById('country').value;
	        if (country == 'all') {
	          autocomplete.setComponentRestrictions([]);
	          map.setCenter({lat: 15, lng: 0});
	          map.setZoom(2);
	        } else {
	          autocomplete.setComponentRestrictions({'country': country});
	          map.setCenter(countries[country].center);
	          map.setZoom(countries[country].zoom);
	        }
	        clearResults();
	        clearMarkers();
	      }

	    function clearMarkers() {
	        for (var i = 0; i < markers.length; i++) {
	          if (markers[i]) {
	            markers[i].setMap(null);
	          }
	        }
	        markers = [];
	      }
	    function dropMarker(i) {
	        return function() {
	          markers[i].setMap(map);
	        };
	      }
	    function clearResults() {
	        var results = document.getElementById('results');
	        while (results.childNodes[0]) {
	          results.removeChild(results.childNodes[0]);
	        }
	      }

	    function showInfoWindow() {
	        var marker = this;
	              infoWindow.open(map, marker);
	              buildIWContent(marker);
	    }
	    function addResult(result, i) {
	        var results = document.getElementById('results');
	        var markerLetter = String.fromCharCode('A'.charCodeAt(0) + i);
	        var markerIcon = MARKER_PATH + markerLetter + '.png';

	        var tr = document.createElement('tr');
	        tr.style.backgroundColor = (i % 2 === 0 ? '#F0F0F0' : '#FFFFFF');
	        tr.onclick = function() {
	          google.maps.event.trigger(markers[i], 'click');
	        };

	        var iconTd = document.createElement('td');
	        var nameTd = document.createElement('td');
	        var icon = document.createElement('img');
	        icon.src = markerIcon;
	        icon.setAttribute('class', 'placeIcon');
	        icon.setAttribute('className', 'placeIcon');
	        var name = document.createTextNode(result.headline);
	        iconTd.appendChild(icon);
	        nameTd.appendChild(name);
	        tr.appendChild(iconTd);
	        tr.appendChild(nameTd);
	        results.appendChild(tr);
	      }
	    function buildIWContent(marker) {
	        document.getElementById('iw-url').innerHTML = '<b><a href="#/international/' + marker.placeResult.id_new +
	            '">' + marker.placeResult.headline + '</a></b>';

	        	document.getElementById('iw-address').textContent = marker.placeResult.headline;
	          document.getElementById('iw-phone-row').style.display = '';
	          document.getElementById('iw-phone').textContent =
	              marker.placeResult.author;
	        if (marker.placeResult.main_group == "CUL"){
	          document.getElementById('iw-rating-row').style.display = '';
	          document.getElementById('iw-rating').innerHTML = "Cultura";
	        }else if (marker.placeResult.main_group == "INT") {
	          document.getElementById('iw-rating-row').style.display = '';
	          document.getElementById('iw-rating').innerHTML = "Internacional";
	        }else if (marker.placeResult.main_group == "ESP") {
	          document.getElementById('iw-rating-row').style.display = '';
	          document.getElementById('iw-rating').innerHTML = "Espanya";
	        }else if (marker.placeResult.main_group == "DEP") {
	          document.getElementById('iw-rating-row').style.display = '';
	          document.getElementById('iw-rating').innerHTML = "Deportes";
	        }else if (marker.placeResult.main_group == "ECO") {
	          document.getElementById('iw-rating-row').style.display = '';
	          document.getElementById('iw-rating').innerHTML = "Economía";
	        }

	        document.getElementById('iw-website').textContent = marker.placeResult.category;
	      }
	}]);