AvatoNews.controller('recoverCtrl', function($scope, services, CommonService, $location){
	$scope.recover_email = function(){
		var info = {email : $scope.recover.email};

		services.post('login', 'search_email', info).then(function(response){

			if (response.success == true){
				CommonService.banner("Enviado email", "Add");
				$location.path("/");
			}else{
				if(response.error == 1){
					$scope.error_recover_email = true;
					$scope.span_recover_email = "El email no existe en la base de datos";
				}else if(response.error == 2){
					$scope.error_revover_email = true;
					$scope.span_recover_email = "El email no ha sido activado";
				}
			}
		});
	}
});

AvatoNews.controller('changePassCtrl', function($scope, services, CommonService, $route, $location){
	$scope.span_changepass_password = "Introduzca contraseña";
	$scope.span_changepass_password2 = "Vuelva a introducir la contraseña";
	var token = $route.current.params.token;

	$scope.update_password = function(){

		if(validate_changePass()){

			var data = {token: token, password: $scope.change.password};
			var info = JSON.stringify(data);

			services.post("login","update_password", info).then(function(response){
				console.log(response);
				if(response.success == true){
					CommonService.banner("Contraseña modificada con éxito", "Add");
					$location.path("/");
				}else if(response.success == false){
					if(response.error == 1){
						CommonService.banner("No se ha podido modificar la contraseña, si este error persiste contacte con un administrador", "Add");
						$location.path("/");
					}else if(response.error == 2){
						CommonService.banner("Código de cambio de contraseña no encontrado, contacte con un administrador", "Add");
						$location.path("/");
					}
				}
			});

		}
	}

	function validate_changePass(){
		var confirm = true;

		if($scope.change.password.length < 6){
			confirm = false;
			$scope.changepass = true;
			$scope.span_changepass_password = "La contraseña debe contener más de 5 carácteres";
		}
		if($scope.change.password != $scope.change.password2){
			confirm = false;
			$scope.changepass2 = true;
			$scope.span_changepass_password2 = "Las contraseñas deben conincidir";			
		}
		return confirm;
	}
});