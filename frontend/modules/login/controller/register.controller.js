AvatoNews.controller('registerCtrl', function($scope, services, CommonService, $location){
	$scope.span_register_name = 'Introduzca nombre de usuario';
	$scope.span_register_email = 'Introduzca email de contacto';
	$scope.span_register_password = 'Introduzca contraseña';
	$scope.span_register_password2 = 'Vuelva a introducir la contraseña';


	$scope.submitRegister = function(){
		if(validate_register($scope.register)){
			var user = JSON.stringify($scope.register);
			services.post('login','save_user', $scope.register).then(function(response){
				if(response.success == true){
					CommonService.banner("Hemos enviado un correo de confirmación a su correo", "Add");
					$location.path("/");
				}else{
					if(response.error == 1){
						$scope.span_register_name = 'Nombre de usuario ya en uso';
						$scope.register_name = true;
					}else if(response.error == 2){
						$scope.span_register_email = 'Email ya en uso';
						$scope.register_email = true;
					}else if(response.error == 3){
						CommonService.banner("Problemas con el servidor, intentelo más tarde", "Add");
					}
				}
			});
		}

	}

	function validate_register(register){
		var confirm = true;
		if(register.name.length <= 3){
			confirm = false;
			$scope.span_register_name = "El nombre debe tener más de 3 carácteres";
			$scope.register_name = true;
		}
		if(register.password.length < 6){
			confirm = false;
			$scope.span_register_password = "La contraseña debe contener mínimo 6 carácteres";
			$scope.register_password = true;
		}
		if(register.password != register.password2){
			confirm = false;
			$scope.span_register_password2 = "Las contraseñas deben coincidir";
			$scope.register_password2 = true;
		}
		return confirm;
	}
});

AvatoNews.controller('confirmCtrl', function($scope, $route, CommonService, services){
	var token = $route.current.params.token;

	if (token.substring(0, 3) !== 'Ver') {
        CommonService.banner("Ha habido algún tipo de error con la dirección", "Err");
        $location.path('/');
    }else{
    	services.get("login","confirm_email", token).then(function(response){

    		if(response.success == true){
    			CommonService.banner("Email confirmado con éxito");
    			$location.path("/");
    		}else if(response.success == false){
    			if(response.error == 1){
    				CommonService.banner("Codigo erroneo, si continua apareciendo este error contacte con un administrador");
    				$location.path("/");
    			}else if(response.error == 2){
    				CommonService.banner("Código de activación no encontrado");
    				$location.path("/");
    			}else if(response.error == 3){
    				CommonService.banner("Error inesperado, vuelva a intentarlo más tarde");
    				$location.path("/");
    			}
    		}
    	});
    }
});