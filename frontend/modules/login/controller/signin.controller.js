AvatoNews.controller('loginCtrl', function($scope, social_networks, userService, $rootScope, $location){
	$scope.span_sign_in_name = "El usuario no puede estar vacio";
	$scope.span_sign_in_password = "Introduzca la contraseña";

	$scope.sign_in_fb = function() {
		social_networks.fb();
	}

	$scope.sign_in_tw = function() {
		social_networks.tw();
	}

	$scope.sign_in_g = function() {
		social_networks.g();
	}

	$scope.sign_in_manual = function(){
		if(userService.validate_form($scope.sign_in.name, $scope.sign_in.password)){
			userService.manual_login($scope.sign_in.name, $scope.sign_in.password);
		}
	}

	$scope.clean = function(){
		$scope.sign_in_password = false;
		$scope.sign_in_name = false;
	}
});

AvatoNews.controller('profileCtrl', function(user, $scope, $location, services, userService, localStorage, $route, load_select, CommonService, comment, likes, ranks){
    $scope.ranks = ranks;
    $scope.likes = likes;
	$scope.comment = comment;
    if(!user){
		$location.path("/");
	}else{
		$scope.user = user;
	}

    load_select.load_country()
    .then(function (response) {
        if(response.success){
            $scope.paises = response.datas;
        }else{
            $scope.AlertMessage = true;
            $scope.user.pais_error = "Error al recuperar la informacion de paises";
            $timeout(function () {
                $scope.user.pais_error = "";
                $scope.AlertMessage = false;
            }, 2000);
        }
    });

        $scope.resetcountry = function () {
        if ($scope.user.pais.sISOCode == 'ES') {
            load_select.loadProvince()
            .then(function (response) {
                if(response.success){
                    $scope.provincias = response.datas;
                }else{
                    $scope.AlertMessage = true;
                    $scope.user.prov_error = "Error al recuperar la informacion de provincias";
                    $timeout(function () {
                        $scope.user.prov_error = "";
                        $scope.AlertMessage = false;
                    }, 2000);
                }
            });
            $scope.poblaciones = null;
        }
    };

    $scope.resetValues = function () {
        var datos = {idPoblac: $scope.user.provincia.id};
        load_select.loadTown(datos)
        .then(function (response) {
            if(response.success){
                $scope.poblaciones = response.datas;
            }else{
                $scope.AlertMessage = true;
                $scope.user.pob_error = "Error al recuperar la informacion de poblaciones";
                $timeout(function () {
                    $scope.user.pob_error = "";
                    $scope.AlertMessage = false;
                }, 2000);
            }
        });
    };

	  $scope.dropzoneConfig = {
        'options': {
            'url': 'backend/index.php?module=news&function=upload',
            addRemoveLinks: true,
            maxFileSize: 1000,
            dictResponseError: "Ha ocurrido un error en el server",
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd'
        },
        'eventHandlers': {
            'sending': function (file, formData, xhr) {},
            'success': function (file, response) {

                response = JSON.parse(response);

                if (response.result) {
                    $(".msg").addClass('msg_ok').removeClass('msg_error').text('Success Upload image!!');
                    $('.msg').animate({'right': '300px'}, 300);

                    $scope.user[0].photo = "http://127.0.0.1/" + response.data;


                } else {
                    $(".msg").addClass('msg_error').removeClass('msg_ok').text(response['error']);
                    $('.msg').animate({'right': '300px'}, 300);
                }
            },
            'removedfile': function (file, serverFileName) {
                if (file.xhr.response) {
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    var data = jQuery.parseJSON(file.xhr.response);
                    services.post("news", "delete", JSON.stringify({'filename': data}));
                }
            }
    }};

    $scope.update_profile = function(){
        var person = user[0].name_user;
        var real_name = $scope.user.real_name;
        var real_surname = $scope.user.real_surname;
        var photo = $scope.user[0].photo;
        var date_birth = $scope.user.date_birth;

        var town = $scope.user.poblacion.poblacion;

        var province = $scope.user.provincia.nombre;

        var country = $scope.user.pais.sName; 

        var data = {user : person, real_name : real_name, real_surname : real_surname, birthday : date_birth, photo : photo, country : country, province : province, town : town};
        var info = JSON.stringify(data);

        services.put("login", "update_profile", info).then(function(response){
            console.log(response);

            if(response.success == true){
                CommonService.banner("Usuario modificado con exito", "Add");
                $location.path("/");
            }else{
                CommonService.banner("Usuario no se ha podido modificar", "Add");
            }
        });
    }

    $scope.show_data = function(tags){
        var i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    document.getElementById(tags).style.display = "block";
    }

});