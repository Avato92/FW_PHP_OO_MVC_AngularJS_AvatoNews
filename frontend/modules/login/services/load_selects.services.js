AvatoNews.factory("load_select", ['services', '$q',
function(services, $q) {
	var service = {};
	service.load_country = load_country;
	service.loadProvince = loadProvince;
	service.loadTown = loadTown;

	return service;

	function load_country(){
 		var deferred = $q.defer();
        services.get("login", "load_country").then(function (data) {

            if (data === 'error') {
                deferred.resolve({ success: false, datas: "error_load_pais" });
            } else {
                deferred.resolve({ success: true, datas: data });
            }
        });
        return deferred.promise;
	}

	    function loadProvince() {
        var deferred = $q.defer();
        services.get("login", "load_provinces").then(function (data) {

            if (data === 'error') {
                deferred.resolve({ success: false, datas: "error_load_provincias" });
            } else {
                deferred.resolve({ success: true, datas: data.provincias });
            }
        });
        return deferred.promise;
    };

    function loadTown(datos) {
        var deferred = $q.defer();
        services.post("login", "load_towns", datos).then(function (data) {
 
            if (data === 'error') {
                deferred.resolve({ success: false, datas: "error_load_poblaciones" });
            } else {
                deferred.resolve({ success: true, datas: data.poblaciones });
            }
        });
        return deferred.promise;
    };
}]);