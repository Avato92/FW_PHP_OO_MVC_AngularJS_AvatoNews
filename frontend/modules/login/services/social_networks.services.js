AvatoNews.factory("social_networks", ['$rootScope', 'localStorage', 'services', 'CommonService', 'userService',
	function($rootScope, localStorage, services, CommonService, userService){
		var config = {
	    	apiKey: "AIzaSyDywM2qWpzoMAEbSOrX1dJIqlEYHJS_tdU",
	    	authDomain: "avatonews-angularjs.firebaseapp.com",
	    	databaseURL: "https://avatonews-angularjs.firebaseio.com",
	    	projectId: "avatonews-angularjs",
	    	storageBucket: "avatonews-angularjs.appspot.com",
	    	messagingSenderId: "826788265756"
  		};
  		firebase.initializeApp(config);

  		var social_network = {};
  		social_network.fb = fb;
  		social_network.tw = tw;
  		social_network.g = g;

  		return social_network;

  		function fb(){
			var provider = new firebase.auth.FacebookAuthProvider();

        	var authService = firebase.auth();

  		 	authService.signInWithPopup(provider)
            .then(function(result) {

            var person = {name:result.user.displayName, email:result.user.email, photo:result.user.photoURL, id:result.user.providerData[0].uid};
            var user = JSON.stringify(person);
            sign_in_social_network(user);
			})
                .catch(function(error) {
                console.log('Detectado un error:', error);
			});
		}

		function tw(){
			var provider = new firebase.auth.TwitterAuthProvider();
      		var authService = firebase.auth();
  
          	authService.signInWithPopup(provider).then(function(result) {
          	var person = {name:result.user.displayName, email:result.user.email, photo:result.user.photoURL, id:result.user.providerData[0].uid};
            var user = JSON.stringify(person);
            sign_in_social_network(user);
          	}).catch(function(error) {
               console.log('Detectado un error:', error);
        });
		}

		function g(){
			var provider = new firebase.auth.GoogleAuthProvider();
        	provider.addScope('email');
    
        	var authService = firebase.auth();
          	authService.signInWithPopup(provider).then(function(result) {
          		var person = {name:result.user.displayName, email:result.user.email, photo:result.user.photoURL, id:result.user.providerData[0].uid};
          		var user = JSON.stringify(person);
              sign_in_social_network(user);
          	}).catch(function(error) {
          		console.log('Detectado un error:', error);
        });
		}
    function sign_in_social_network(user){
      services.post('login','find_social_network', user).then(function(response){

        if(response.new == true){
          CommonService.banner("Usuario registrado correctamente", 'Add');
          localStorage.save('token', response[0].token2);
          userService.login();
        }else{
          CommonService.banner("Bienvenido de vuelta", 'Add');
          localStorage.save('token', response[0].token2);
          userService.login();
        }
      });
    }
	}]);