AvatoNews.factory("userService",['$rootScope', 'localStorage', 'services','$location', 'CommonService',
	function($rootScope, localStorage, services, $location, CommonService){
		var service = {};
		service.login = login;
		service.logout = logout;
		service.validate_form = validate_form;
		service.manual_login = manual_login;

		return service;

		function login(){

			var user = localStorage.retrieve('token');

			if(user){
				var data = {"token" : user};
				var info = JSON.stringify(data);

				services.post("login","pull_user", info).then(function(response){
					$rootScope.name_user = response[0].name_user;
					$rootScope.user_menu = true;
					$rootScope.photo_user = response[0].photo;
					$rootScope.photo_menu = true;
					$rootScope.login_button = false;
					$rootScope.logout_button = true;
				});
				$location.path("/");
			}else{
				$rootScope.login_button = true;
				$rootScope.logout_button = false;
			}
		}

		function logout(){
			localStorage.clearStore('token');
			$rootScope.name_user = "";
			$rootScope.user_menu = false;
			$rootScope.photo_user = "";
			$rootScope.photo_menu = false;
			$rootScope.login_button = true;
			$rootScope.logout_button = false;

			$location.path("/");
		}

		function validate_form(name, password){
			var confirm = true;
			if(!name){
				confirm = false;
		 		$rootScope.sign_in_name = true;
			}if(!password){
				confirm = false;
				$rootScope.sign_in_password = true;
			}
			return confirm;
		}

		function manual_login(name, password){
			var data = {user : name, password : password};
			var info = JSON.stringify(data);
			services.post("login", "sign_in", info).then(function(response){

				if(response.success == true){
					localStorage.save('token', response.token);
					login();
				}else if(response.success == false){
					if(response.error == 1){
						$rootScope.user_not_valid = true;
					}else if(response.error == 2){
						$rootScope.password_not_valid = true;
					}else if(response.error == 3){
						CommonService.banner("Error con el servidor, intentelo más tarde", "Add");
					}
				}
			});
		}
	}]);