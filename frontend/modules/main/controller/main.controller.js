AvatoNews.controller('mainCtrl', function ($scope, categories, services) {
	$scope.categories = categories;
	$scope.filteredNews = [];
	$scope.list = true;
	$scope.list2 = false;
	$scope.list3 = false;
	$scope.header = "Categorias:";
	$scope.load_more = true;
	$scope.return = false;
	$scope.all_news = true;
	$scope.show_news = false;
	var categoryList = [];
	var i = 0;
	angular.forEach(categories, function(){
		categoryList.push(categories[i].name);
		i ++;
	});



	$scope.search_subcategories = function() {
			$scope.list = false;
			var selected = this.category.cod;
			var array = { "category" : selected };
			var category = JSON.stringify(array);

			services.post('main', 'list_subcategories', category).then(function(response){

				if(response != "error"){
					$scope.subcategories = response;
					$scope.list2 = true;
					$scope.error = false;
					$scope.header = "Subcategorias:";
					$scope.load_more = false;
					$scope.return = true;
				}else{
					$scope.list = true;
					$scope.list2 = false;
					$scope.error_text = "Error, vuelva a intentarlo";
					$scope.error = true;
				}
			});
	}

	$scope.search_specific_categories = function() {

		var info = {"subcategory" : this.subcategory.cod, "category" : this.subcategory.cod_cat};
		$scope.list2 = false;

		var data = JSON.stringify(info);

		services.post('main', 'list_specific_categories', data).then(function(response){

			if(response != "error"){
				$scope.specific_categories = response;
				$scope.list3 = true;
				$scope.error = false;
				$scope.header = "Categorias especificas:";
			}else{
				$scope.specific_categories = "";
				$scope.list2 = true;
				$scope.error_text = "Error, vuelva a intentarlo";
				$scope.error = true;
			}
		});
	}

	$scope.search_all_subcategories = function(){
		$scope.list = false;
		services.post('main', 'list_subcategories').then(function(response){

			if(response != "error"){
				$scope.subcategories = response;
				$scope.list2 = true;
				$scope.error = false;
				$scope.header = "Subcategorias:";
				$scope.load_more = false;
				$scope.return = true;
			}else{
				$scope.list = true;
				$scope.list2 = false;
				$scope.error_text = "Error, vuelva a intentarlo";
				$scope.error = true;
			}
		});
	}

	$scope.back = function(){
		$scope.list2 = false;
		$scope.list3 = false;
		$scope.list = true;
		$scope.header = "Categorias:";
		$scope.error = false;
		$scope.return = false;
		$scope.load_more = true;
		$scope.news_data = false;
		$scope.show_news = false;
	}

	$scope.complete = function(string){
		var category = $scope.text;
		var output = [];
		angular.forEach(categoryList, function(text){
			if(text.toLowerCase().indexOf(string.toLowerCase()) >= 0){
				output.push(text);
			}
		});
	$scope.filterCategory = output;
	}
	$scope.fillTextbox = function(string){
		$scope.text = string;
		$scope.hidethis = true;
	}

	$scope.search_specific_news = function(){
		var info = {"category" : this.specific_category.cod_cat, "subcategory" : this.specific_category.cod_sub, "specific_category" : this.specific_category.cod};
		var data = JSON.stringify(info);
		services.post('main', 'search_specific_news', data).then(function(response){
			$scope.news = response;
			$scope.news_data = true;
			$scope.list3 = false;
			$scope.all_news = false;
		});
	}

	$scope.list_all_news = function(){
		services.post('main', 'list_all_news').then(function(response){

			if (response != ""){
				$scope.all_news = response;
				$scope.show_news = true;
				$scope.currentPage = 1;
				$scope.pageChanged();
			}
		});
	}

	$scope.pageChanged = function() {
	  var startPos = ($scope.currentPage - 1) * 3;
	  $scope.filteredNews = $scope.all_news.slice(startPos, startPos + 3);
	  $scope.list = false;
	  $scope.list2 = false;
	  $scope.list3 = false;
	  $scope.back = true;
	  $scope.load_more = false;
	};
});